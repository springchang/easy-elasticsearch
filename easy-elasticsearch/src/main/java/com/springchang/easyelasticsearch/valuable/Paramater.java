package com.springchang.easyelasticsearch.valuable;

import org.springframework.util.StringUtils;

/**
 * @author : 张翠山
 */
public class Paramater {

    private String index;

    private String idName;

    private Object idValue;

    public Paramater(){
    }

    public Paramater(String index, String idName, Object idValue) {
        this.index = index;
        this.idName = idName;
        this.idValue = idValue;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getIdName() {
        return idName;
    }

    public void setIdName(String idName) {
        this.idName = idName;
    }

    public Object getIdValue() {
        return idValue;
    }

    public void setIdValue(Object idValue) {
        this.idValue = idValue;
    }

    public boolean isEmpty() {
        return StringUtils.isEmpty(idName) && StringUtils.isEmpty(idValue) && idValue == null;
    }
}
