package com.springchang.easyelasticsearch.valuable;

/**
 * @author : 张翠山
 */
public class Constant {

    public static final int DEAULT_PAGE_SIZE = 20;

    public static final int MAX_ES_SIZE = 10000;

    public static final int DEAULT_PAGE = 1;

    public static final int DEAULT_BOOLEAN_NONE = 0;

    public static final int DEAULT_BOOLEAN_TRUE = 0;

    public static final int DEAULT_BOOLEAN_FALSE = 0;
}
