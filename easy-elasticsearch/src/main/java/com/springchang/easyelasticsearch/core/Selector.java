package com.springchang.easyelasticsearch.core;

import com.springchang.easyelasticsearch.valuable.Constant;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import java.util.HashMap;
import java.util.Map;

/**
 * @author : 张翠山
 */
public class Selector {

    private String index;

    private Map<String, Boolean> sortFieldsToAsc = new HashMap<String, Boolean>();

    private BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();

    private String[] includeFields;

    private String[] excludeFields;

    private int size;

    private int from;

    /**
     * @param index 索引
     */
    public Selector(String index) {
        this.index = index;
        boolQueryBuilder.must(QueryBuilders.termQuery("deleted", "false"));
    }

    public Selector(String index, int flag) {
        if(Constant.DEAULT_BOOLEAN_NONE == flag) {
        } else if(Constant.DEAULT_BOOLEAN_TRUE == flag) {
            boolQueryBuilder.must(QueryBuilders.termQuery("deleted", "true"));
        } else if(Constant.DEAULT_BOOLEAN_FALSE == flag) {
            boolQueryBuilder.must(QueryBuilders.termQuery("deleted", "false"));
        } else {
            throw new RuntimeException("flag变量未识别的参数值"+flag);
        }
        this.index = index;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public Map<String, Boolean> getSortFieldsToAsc() {
        return sortFieldsToAsc;
    }

    public void setSortFieldsToAsc(Map<String, Boolean> sortFieldsToAsc) {
        this.sortFieldsToAsc = sortFieldsToAsc;
    }

    public BoolQueryBuilder getBoolQueryBuilder() {
        return boolQueryBuilder;
    }

    public void setBoolQueryBuilder(BoolQueryBuilder boolQueryBuilder) {
        this.boolQueryBuilder = boolQueryBuilder;
    }

    public String[] getIncludeFields() {
        return includeFields;
    }

    public void setIncludeFields(String[] includeFields) {
        this.includeFields = includeFields;
    }

    public String[] getExcludeFields() {
        return excludeFields;
    }

    public void setExcludeFields(String[] excludeFields) {
        this.excludeFields = excludeFields;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }
}
