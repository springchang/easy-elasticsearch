package com.springchang.easyelasticsearch.exception;

/**
 * 自定义异常：索引已经存在
 * @author : 张翠山
 */
public class IndexExistException extends RuntimeException{

    public IndexExistException() {
        super();
    }

    public IndexExistException(String message) {
        super(message);
    }
}
