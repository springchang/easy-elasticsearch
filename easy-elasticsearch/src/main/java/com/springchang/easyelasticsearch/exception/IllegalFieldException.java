package com.springchang.easyelasticsearch.exception;

/**
 * 自定义异常：非法的字段类型
 * @author : 张翠山
 */
public class IllegalFieldException extends RuntimeException{

    public IllegalFieldException() {
        super();
    }

    public IllegalFieldException(String message) {
        super(message);
    }
}
