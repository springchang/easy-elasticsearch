package com.springchang.easyelasticsearch.exception;

/**
 * 自定义异常：索引未找到
 * @author : 张翠山
 */
public class IndexNotExistException extends RuntimeException{

    public IndexNotExistException() {
        super();
    }

    public IndexNotExistException(String message) {
        super(message);
    }
}
