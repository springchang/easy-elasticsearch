package com.springchang.easyelasticsearch.configuration;

import com.springchang.easyelasticsearch.core.RestHighLevelClientPlus;
import com.springchang.easyelasticsearch.properites.CoreProperties;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
@EnableConfigurationProperties(CoreProperties.class)
public class CoreConfiguration {

    @Autowired
    CoreProperties coreProperties;

    /**
     * 配置初始化
     */
    @PostConstruct
    public void init() {
        // 打印 Banner
        System.out.println("                                         ");
        System.out.println("---   --     ---  \\  /       ---   ---  ");
        System.out.println("|    /   \\   |     ---       |     |    ");
        System.out.println("|--  |   |   |__    |        |--   |__  ");
        System.out.println("|    |---|      |   |        |        | ");
        System.out.println("|__  |   |    __|   |        |__    __| ");
        System.out.println(":: Easy ElasticSearch :: 张翠山 :: (v0.0.1)");
        System.out.println("                                          ");
    }

    /*向Spring 容器注入RestHighLevelClient*/
    @Bean(name="restHighLevelClient")
    public RestHighLevelClient getClient() {
        String[] hosts = coreProperties.getHost();
        HttpHost[] httpHosts = new HttpHost[hosts.length];
        for(int i=0;i<hosts.length;i++) {
            httpHosts[i] = new HttpHost(hosts[i], coreProperties.getHttpPort(), "http");
        }

        final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(coreProperties.getAuthUsername(), coreProperties.getAuthPassword()));

        RestClientBuilder builder = RestClient.builder(httpHosts).setRequestConfigCallback(new RestClientBuilder.RequestConfigCallback() {
            @Override
            public RequestConfig.Builder customizeRequestConfig(RequestConfig.Builder requestConfigBuilder) {
                requestConfigBuilder.setConnectTimeout(-1);
                requestConfigBuilder.setSocketTimeout(-1);
                requestConfigBuilder.setConnectionRequestTimeout(-1);
                return requestConfigBuilder;
            }
        }).setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
            @Override
            public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpClientBuilder) {
                httpClientBuilder.disableAuthCaching();
                return httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
            }
        });

        //setMaxRetryTimeoutMillis(5*60*1000);

        RestHighLevelClient client = new RestHighLevelClient(builder);
        return client;
    }

    @Bean(name = "restHighLevelClientPlus")
    public RestHighLevelClientPlus getRestHighLevelClientPlus(RestHighLevelClient restHighLevelClient) {
        RestHighLevelClientPlus restHighLevelClientPlus = new RestHighLevelClientPlus();
        restHighLevelClientPlus.setRestHighLevelClient(restHighLevelClient);
        return restHighLevelClientPlus;
    }
}
