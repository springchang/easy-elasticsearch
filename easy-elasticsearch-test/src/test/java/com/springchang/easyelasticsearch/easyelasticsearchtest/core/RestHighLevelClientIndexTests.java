package com.springchang.easyelasticsearch.easyelasticsearchtest.core;

import com.springchang.easyelasticsearch.core.RestHighLevelClientPlus;
import com.springchang.easyelasticsearch.easyelasticsearchtest.pojo.Student;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * 功能：测试索引是否存在，已经自动化生存索引
 * 作者：张翠山
 * 日期：2021/08/18
 */
@SpringBootTest
@Slf4j
class RestHighLevelClientIndexTests {

    @Autowired
    RestHighLevelClientPlus restHighLevelClientPlus;

    @Test
    void contextLoads() {
        System.out.println("Spring Context is Loaded...");
    }

    /**
     * 功能：单元测试，查询指定索引是否存在
     * 作者：张翠山
     * 日期：2021/08/18
     * @throws Exception
     */
    @Test
    void testIndexExistx() throws Exception {

        //1.定义待判断的索引名
        String index = "jk_v1_student";

        //2. 查询ES库判断是否存在
        boolean exist = restHighLevelClientPlus.isIndexExist(index);

        assertEquals(false, exist);

        log.info("单元测试通过，实际返回值: {}", exist);
    }

    /**
     * 功能：单元测试，创建指定的索引
     * 作者：张翠山
     * 日期：2021/08/18
     * @throws Exception
     */
    @Test
    void testCreateIndex() throws Exception {

        boolean success = restHighLevelClientPlus.creasteIndex(Student.class);

        assertEquals(true, success);

        log.info("单元测试通过，实际返回值: {}", success);
    }
}
