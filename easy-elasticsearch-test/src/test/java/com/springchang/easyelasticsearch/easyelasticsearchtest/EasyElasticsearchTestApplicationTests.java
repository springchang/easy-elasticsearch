package com.springchang.easyelasticsearch.easyelasticsearchtest;

import com.springchang.easyelasticsearch.core.RestHighLevelClientPlus;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.RestHighLevelClient;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Slf4j
class EasyElasticsearchTestApplicationTests {

    @Autowired
    RestHighLevelClient restHighLevelClient;

    @Autowired
    RestHighLevelClientPlus restHighLevelClientPlus;

    @Test
    void contextLoads() {
        System.out.println("Spring Context is Loaded...");
    }

    @Test
    void testRestHighLevelClient() throws Exception {
        String str = restHighLevelClient.toString();
        log.info("str = {}", str);
    }

    @Test
    void testRestHighLevelClientPlus() throws Exception {
        String str = restHighLevelClientPlus.toString();
        log.info("str = {}", str);
    }
}
