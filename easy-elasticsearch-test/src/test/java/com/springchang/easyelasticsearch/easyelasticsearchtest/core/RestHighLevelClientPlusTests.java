package com.springchang.easyelasticsearch.easyelasticsearchtest.core;

import com.springchang.easyelasticsearch.core.RestHighLevelClientPlus;
import com.springchang.easyelasticsearch.core.Selector;
import com.springchang.easyelasticsearch.easyelasticsearchtest.pojo.GraphLog;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.QueryBuilders;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@Slf4j
class RestHighLevelClientPlusTests {

    @Autowired
    RestHighLevelClientPlus restHighLevelClientPlus;

    @Test
    void contextLoads() {
        System.out.println("Spring Context is Loaded...");
    }

    /**
     * 功能：单元测试，查询list的map数据
     * 作者：张翠山
     * 日期：2021/08/18
     * @throws Exception
     */
    @Test
    void testGetListToMap() throws Exception {
        Selector selector = new Selector("jk_v1_graph_log");
        List<Map<String, Object>> list = restHighLevelClientPlus.getList(selector);
        assertEquals(20, list.size());
    }

    /**
     * 功能：单元测试，查询list Bean 的数据
     * 作者：张翠山
     * 日期：2021/08/18
     * @throws Exception
     */
    @Test
    void testGetListToBean() throws Exception {
        Selector selector = new Selector("jk_v1_graph_log");
        List<GraphLog> list = restHighLevelClientPlus.getList(selector, GraphLog.class);
        assertEquals(20, list.size());
    }

    /**
     * 功能：单元测试，测试查询一条数据
     * 作者：张翠山
     * 日期：2021/08/18
     * @throws Exception
     */
    @Test
    void testGetOneToBean() throws Exception {
        Selector selector = new Selector("jk_v1_graph_log");
        GraphLog graphLog = restHighLevelClientPlus.getOne(selector, GraphLog.class);
        assertNotNull(graphLog);
    }

    /**
     * 功能：单元测试，测试查询一条数据
     * 作者：张翠山
     * 日期：2021/08/18
     * @throws Exception
     */
    @Test
    void testGetOneToMap() throws Exception {
        Selector selector = new Selector("jk_v1_graph_log");
        Map<String, Object> graphLogMap = restHighLevelClientPlus.getOne(selector);
        assertNotNull(graphLogMap);
        assertEquals(graphLogMap.size() > 0, true);
        log.info("测试案例通过");
    }

    /**
     * 功能：单元测试，测试查询记录总数
     * 作者：张翠山
     * 日期：2021/08/18
     * @throws Exception
     */
    @Test
    void testCount() throws Exception {
        Selector selector = new Selector("jk_v1_graph_log");
        long total = restHighLevelClientPlus.count(selector);
        assertEquals(6685L, total);
        log.info("测试案例通过, 实际返回结果： {}", total);
    }

    /**
     * 功能：单元测试,测试新增记录数
     * 作者：张翠山
     * 日期：2021/08/18
     * @throws Exception
     */
    @Test
    void testCreate() throws Exception {
        GraphLog graphLog = new GraphLog();
        graphLog.setLog_id(10086L);
        graphLog.setDeleted(false);
        graphLog.setModule("测试模块");
        graphLog.setOperation("测试新增");
        graphLog.setRaw_content("这是原始内容");
        graphLog.setUpdate_content("这是新增的内容");
        graphLog.setLast_update(new Date());
        graphLog.setUser_id(-1L);

        boolean success = restHighLevelClientPlus.create(graphLog);
        assertEquals(true, success);
        log.info("测试案例通过, 实际返回结果： {}", success);
    }

    /**
     * 功能：单元测试,测试新增记录数
     * 作者：张翠山
     * 日期：2021/08/18
     * @throws Exception
     */
    @Test
    void testUpdate() throws Exception {

        Selector selector = new Selector("jk_v1_graph_log");
        selector.getBoolQueryBuilder().must(QueryBuilders.termQuery("log_id", 10086));
        GraphLog graphLog =restHighLevelClientPlus.getOne(selector, GraphLog.class);

        graphLog.setUpdate_content("测试更新数据");

        boolean success = restHighLevelClientPlus.update(graphLog);
        assertEquals(true, success);
        log.info("测试案例通过, 实际返回结果： {}", success);
    }
}
