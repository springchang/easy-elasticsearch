package com.springchang.easyelasticsearch.easyelasticsearchtest.pojo;

import com.springchang.easyelasticsearch.annotation.EsField;
import com.springchang.easyelasticsearch.annotation.EsIndex;
import com.springchang.easyelasticsearch.annotation.EsLogicId;
import com.springchang.easyelasticsearch.valuable.EsTypes;
import lombok.Data;

/**
 * 索引测试类,测试Student能不能在ES库中生存对应的index
 * @date   : 2021/08/18
 * @author : 张翠山
 */
@Data
@EsIndex(name = "jk_v1_student")
public class Student {

    @EsLogicId(name = "student_id")
    @EsField(column_name = "student_id", type = EsTypes.LONG)
    private long studentId;

    @EsField(column_name = "deleted", type = EsTypes.BOOLEAN)
    private boolean deleted;

    @EsField(column_name = "student_name", type = EsTypes.TEXT)
    private String studentName;

    @EsField(column_name = "student_address", type = EsTypes.TEXT)
    private String studentAddress;
}
