package com.springchang.easyelasticsearch.easyelasticsearchtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasyElasticsearchTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(EasyElasticsearchTestApplication.class, args);
    }
}
