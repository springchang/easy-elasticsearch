package com.springchang.easyelasticsearch.easyelasticsearchtest.pojo;

import com.springchang.easyelasticsearch.annotation.EsField;
import com.springchang.easyelasticsearch.annotation.EsLogicId;
import com.springchang.easyelasticsearch.annotation.EsIndex;
import com.springchang.easyelasticsearch.annotation.EsLogicDelete;
import com.springchang.easyelasticsearch.valuable.EsTypes;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import java.util.Date;

/**
 * @author : 张翠山
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EsIndex(name = "jk_v1_graph_log")
public class GraphLog {

    @EsLogicId(name = "log_id")
    @EsField(column_name = "log_id", type = EsTypes.LONG)
    private long log_id;

    @EsLogicDelete(column_name = "deleted", type = EsTypes.BOOLEAN)
    @EsField(column_name = "deleted", type = EsTypes.BOOLEAN)
    private boolean deleted;

    @EsField(column_name = "entity_id", type = EsTypes.LONG)
    private long entity_id;

    @EsField(column_name = "raw_content", type = EsTypes.TEXT)
    private String raw_content;

    @EsField(column_name = "relation_id", type = EsTypes.LONG)
    private long relation_id;

    @EsField(column_name = "update_content", type = EsTypes.TEXT)
    private String update_content;

    @EsField(column_name = "user_id", type = EsTypes.LONG)
    private long user_id;

    @EsField(column_name = "last_update", type = EsTypes.DATE)
    private Date last_update;

    @EsField(column_name = "module", type = EsTypes.TEXT)
    private String module;

    @EsField(column_name = "operation", type = EsTypes.TEXT)
    private String operation;
}
